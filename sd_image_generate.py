import json
import os
import numpy as np
import cv2
import shutil
import tqdm
import torch

from diffusers import AutoPipelineForImage2Image, StableDiffusionXLPipeline
from diffusers.utils import make_image_grid, load_image
pipeline = AutoPipelineForImage2Image.from_pretrained(
    "stabilityai/stable-diffusion-xl-refiner-1.0", torch_dtype=torch.float16, variant="fp16", use_safetensors=True
)
# pipeline = StableDiffusionXLPipeline.from_pretrained(
#     "stabilityai/stable-diffusion-xl-base-1.0", torch_dtype=torch.float16, variant="fp16", use_safetensors=True
# ).to("cuda")
pipeline.enable_model_cpu_offload()


result_file = "/home/jiangxue/lmms-eval/logs/0618_2215_llava_v1.5_coco_chair_greedy_anchor_llava_model_args_7adc68/coco_chair.json"
coco_image_dir = "/data/jiangxue/pope/val2014"
output_dir = "/ssd2/jiangxue/sd_coco_empty_caption"
os.makedirs(output_dir, exist_ok=True)

resp = json.load(open(result_file))["logs"]

for resp_single in tqdm.tqdm(resp):
    file_name = resp_single["doc"]["file_name"]
    # caption = resp_single["filtered_resps"][0]
    caption = ""
    init_image = load_image(os.path.join(coco_image_dir, file_name))
    prompt = caption
    image = pipeline(prompt, image=init_image, strength=0.5, guidance_scale=8.0).images[0]
    # image = pipeline(prompt).images[0]
    shutil.copy(os.path.join(coco_image_dir, file_name), os.path.join(output_dir, os.path.splitext(file_name)[0] + "_orig.jpg"))
    image.save(os.path.join(output_dir, file_name))
    with open(os.path.join(output_dir, os.path.splitext(file_name)[0] + "_caption.txt"), "w") as f:
        f.write(caption)
    

# for visual in visuals:
#     init_image = load_image(visual)
#     prompt = text_outputs
#     image = pipeline(prompt, image=init_image, strength=0.5, guidance_scale=8.0).images[0]
