
# accelerate launch --num_processes=2 --main_process_port 12335 -m lmms_eval --model llava \
# --model_args pretrained="liuhaotian/llava-v1.5-7b,use_flash_attention_2=False" \
# --tasks pope --batch_size 1 --log_samples --log_samples_suffix llava_v1.5_pope_vcd_temper1 --output_path ./logs/;


# accelerate launch --num_processes=2 --main_process_port 12225 -m lmms_eval --model llava \
# --model_args pretrained="liuhaotian/llava-v1.5-7b,use_flash_attention_2=False" \
# --tasks mme --batch_size 1 --log_samples --log_samples_suffix llava_v1.5_mme_vcd --output_path ./logs/;

# accelerate launch --num_processes=2 --main_process_port 12225 -m lmms_eval --model llava \
# --model_args pretrained="liuhaotian/llava-v1.5-7b,use_flash_attention_2=False" \
# --tasks coco_cap --batch_size 1 --log_samples --log_samples_suffix llava_v1.5_coco --output_path ./logs/;

# CUDA_VISIBLE_DEVICES=5,6 accelerate launch --num_processes=2 --main_process_port 12225 -m lmms_eval --model llava \
# --model_args pretrained="liuhaotian/llava-v1.5-7b,use_flash_attention_2=False" \
# --tasks coco_chair --batch_size 1 --log_samples --log_samples_suffix llava_v1.5_coco_chair --output_path ./logs/ --limit 500;


CUDA_VISIBLE_DEVICES=0,6 accelerate launch --num_processes=2 --main_process_port 12315 -m lmms_eval --model llava \
--model_args pretrained="liuhaotian/llava-v1.5-7b,use_flash_attention_2=False" \
--tasks coco_chair --batch_size 1 --log_samples --log_samples_suffix llava_v1.5_coco_custom_anchor --output_path ./logs/ --limit 500 --shuffle_seed 777;