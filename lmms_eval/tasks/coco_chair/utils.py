import os
import json
from pycocoevalcap.eval import COCOEvalCap, Bleu, Meteor, Rouge, Cider, Spice
from pycocoevalcap.tokenizer.ptbtokenizer import PTBTokenizer
from pycocotools.coco import COCO

from lmms_eval.tasks._task_utils.file_utils import generate_submission_file
from lmms_eval.tasks._task_utils.chair import CHAIR

import logging

eval_logger = logging.getLogger("lmms-eval")

dir_name = os.path.dirname(os.path.abspath(__file__))

COCO_METRICS = ["chairi", "chairs", "recall", "len"]  # , "SPICE"]

# chair_scores = dict()


def coco_doc_to_visual(doc):
    return [doc["image"].convert("RGB")]


def coco_doc_to_text(doc):
    return f"Provide a one-sentence caption for the provided image."


def coco_process_result(doc, result):
    """
    Args:
        doc: a instance of the eval dataset
        results: [pred]
    Returns:
        a dictionary with key: metric name, value: metric value
    """
    pred = result[0] if len(result) > 0 else ""
    question_id = doc["question_id"]
    # The question id in our dataset is the image file itself
    image_id = int(question_id.split("_")[-1].split(".")[0])
    id = doc["id"]

    data_dict = {"answer": doc["answer"], "pred": pred, "image_id": image_id, "id": id}

    return {f"coco_{metric}": data_dict for metric in COCO_METRICS}

def coco_chairi(results, args):

    # In order to make the coco eval tools to successfully create index
    # We need at least two dict in the dataset
    # 'annotation' and 'images'
    # 'annotation' exactly reproduce the original annotation
    # 'images' however only need the image id which is contained in the file name

    # dataset = {"annotations": [], "images": []}
    # idx = 0
    # for result in results:
    #     stored_results.append({"image_id": int(result["image_id"]), "caption": result["pred"]})
    #     for a in result["answer"]:
    #         dataset["annotations"].append({"image_id": int(result["image_id"]), "caption": a, "id": idx})
    #         idx += 1
    #     dataset["images"].append({"id": result["image_id"]})



    eval_logger.info(f"Computing CHAIR scores...")
    eval_pairs = dict(captions=[],
                      image_ids=[])
    for result in results:
        eval_pairs["captions"].append(result["pred"])
        eval_pairs["image_ids"].append(result["image_id"])

    evaluator = CHAIR("/home/jiangxue/lmms-eval/lmms_eval/tasks/coco_chair", eval_pairs["image_ids"])

    cap_dict = evaluator.compute_chair(**eval_pairs) 

    # global chair_scores 
    chair_scores = cap_dict
    os.makedirs(args.output_path, exist_ok=True)
    with open(os.path.join(args.output_path, "chair_dump.json"), "w") as f:
        json.dump(cap_dict, f)

    return chair_scores['overall_metrics']['CHAIRi']

def coco_chairs(results, args):
    with open(os.path.join(args.output_path, "chair_dump.json"), "r") as f:
        chair_scores=json.load(f)
    return chair_scores['overall_metrics']['CHAIRs']

def coco_recall(results, args):
    with open(os.path.join(args.output_path, "chair_dump.json"), "r") as f:
        chair_scores=json.load(f)
    return chair_scores['overall_metrics']['Recall']

def coco_len(results, args):
    with open(os.path.join(args.output_path, "chair_dump.json"), "r") as f:
        chair_scores=json.load(f)
    return chair_scores['overall_metrics']['Len']

