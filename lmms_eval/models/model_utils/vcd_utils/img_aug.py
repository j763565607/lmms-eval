from imgaug import augmenters as iaa
import numpy as np
import cv2
import os
from PIL import Image


def img_aug(img, selected_augmentation):
    # 读取图像
    # image = cv2.imread(img)
    if selected_augmentation == 'multiply':
        aug = iaa.Sequential([iaa.Multiply(1.1)])  # 将乘法因子固定为1.1
        aug_name = "Multiply"
    elif selected_augmentation == 'contrast':
        aug = iaa.Sequential([iaa.contrast.LinearContrast(1.1)])  # 将对比度调整因子固定为1.1
        aug_name = "Contrast"
    elif selected_augmentation == 'sharpen':
        aug = iaa.Sequential([iaa.Sharpen(alpha=0.5, lightness=1.0)])  # 将锐化参数固定为0.5和1.0
        aug_name = "Sharpen"
    elif selected_augmentation == 'blur':
        aug = iaa.Sequential([iaa.GaussianBlur(sigma=1.5)])  # 将高斯模糊的标准差固定为1.5
        aug_name = "Blur"
    elif selected_augmentation == 'noise':
        aug = iaa.Sequential([iaa.AdditiveGaussianNoise(scale=0.02*255)])  # 将高斯噪声的标准差固定为0.02*255
        aug_name = "Noise"
    elif selected_augmentation == 'colorspace':
        aug = iaa.Sequential([iaa.WithColorspace(to_colorspace="HSV", from_colorspace="RGB",
                        children=iaa.WithChannels(0, iaa.Add(30)))])  # 将色彩空间变换参数固定为30
        aug_name = "Colorspace"
    elif selected_augmentation == 'translate':
        aug = iaa.Sequential([iaa.Affine(translate_percent={"x": 0.1, "y": 0.1})])  # 将平移参数固定为0.1
        aug_name = "Translate"
    elif selected_augmentation == 'rotate':
        aug = iaa.Sequential([iaa.Affine(rotate=45)])  # 将旋转角度固定为45度
        aug_name = "Rotate"
    elif selected_augmentation == 'flip_lr':
        aug = iaa.Sequential([iaa.Fliplr(1.0)])  # 左右翻转
        aug_name = "Flip Left-Right"
    elif selected_augmentation == 'flip_ud':
        aug = iaa.Sequential([iaa.Flipud(1.0)])  # 上下翻转
        aug_name = "Flip Up-Down"
    elif selected_augmentation == 'scale':
        aug = iaa.Sequential([iaa.Affine(scale={"x": 1.1, "y": 1.1})])  # 将缩放参数固定为1.1
        aug_name = "Scale"
    elif selected_augmentation == 'contrast_norm':
        aug = iaa.Sequential([iaa.ContrastNormalization(1.1)])  # 将对比度正规化参数固定为1.1
        aug_name = "Contrast Normalization"
    elif selected_augmentation == 'motion_blur':
        aug = iaa.Sequential([iaa.MotionBlur(k=5)])  # 将运动模糊参数固定为5
        aug_name = "Motion Blur"
    elif selected_augmentation == 'emboss':
        aug = iaa.Sequential([iaa.Emboss(alpha=0.5, strength=1.0)])  # 将浮雕参数固定为0.5和1.0
        aug_name = "Emboss"
    elif selected_augmentation == 'elastic':
        aug = iaa.Sequential([iaa.ElasticTransformation(alpha=2.0, sigma=0.25)])  # 将弹性变换参数固定为2.0和0.25
        aug_name = "Elastic Transformation"
    elif selected_augmentation == 'crop':
        aug = iaa.Sequential([iaa.Crop(percent=0.2)])  # 将裁剪百分比固定为0.2
        aug_name = "Crop"
    elif selected_augmentation == 'pad':
        aug = iaa.Sequential([iaa.Pad(percent=0.1)])  # 将填充百分比固定为0.1
        aug_name = "Pad"
    elif selected_augmentation == 'shear':
        aug = iaa.Sequential([iaa.Affine(shear=10)])  # 将剪切参数固定为10度
        aug_name = "Shear"
    elif selected_augmentation == 'sigmoid_contrast':
        aug = iaa.Sequential([iaa.SigmoidContrast(gain=6, cutoff=0.5)])  # 将 Sigmoid 对比度参数固定为6和0.5
        aug_name = "Sigmoid Contrast"
    elif selected_augmentation == 'piecewise_affine':
        aug = iaa.Sequential([iaa.PiecewiseAffine(scale=0.03)])  # 将分段仿射变换参数固定为0.03
        aug_name = "Piecewise Affine"
    elif selected_augmentation == 'affine':
        aug = iaa.Sequential([iaa.Affine(scale=1.2)])  # 将仿射变换缩放参数固定为1.2
        aug_name = "Affine"
    image_np = np.array(img)

    # 应用增强
    image_aug = aug(image=image_np)
    image_aug = Image.fromarray(image_aug)

    # 应用增强
    # image_aug = aug(image=image)
    return image_aug, aug_name



if __name__ == "__main__":
    all_augmentations = [
    'multiply', 'contrast', 'sharpen', 'blur', 'noise', 'colorspace', 
    'translate', 'rotate', 'flip_lr', 'flip_ud', 'scale',  
    'contrast_norm', 'motion_blur', 'emboss', 'elastic', 'crop', 'pad', 
    'shear', 'sigmoid_contrast', 'piecewise_affine', 'affine']

    # 创建保存增强后图片的文件夹
    output_folder = "/ssd2/jiangxue/augmented_images"
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    # 读取示例图像
    img_path = "/ssd2/jiangxue/MME/MME_Benchmark_release_version/scene/images/Places365_val_00000107.jpg"
    image = cv2.imread(img_path)

    # 遍历所有增强方法并进行增强
    for augmentation in all_augmentations:
        # 执行增强
        augmented_image, aug_name = img_aug(img_path, augmentation)
        
        # 构建输出路径
        output_path = os.path.join(output_folder, f"{aug_name}.jpg")

        # 保存增强后的图像
        cv2.imwrite(output_path, augmented_image)



